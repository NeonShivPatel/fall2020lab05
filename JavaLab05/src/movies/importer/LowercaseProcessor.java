//@author: Shiv Patel
//@ID: 1935098
package movies.importer;

import java.util.ArrayList;

public class LowercaseProcessor extends Processor {

	public LowercaseProcessor(String sourceDir, String destinationDir) {
		super(sourceDir, destinationDir, true);
	}

	@Override
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> asLower = new ArrayList<String>();
		//Loops through the input ArrayList and adds its value to the new asLower ArrayList after becoming lowercase
		for (int i=0; i < input.size(); i++) {
			asLower.add(input.get(i).toLowerCase());
		}
		return asLower;
	}

}