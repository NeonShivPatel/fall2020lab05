//@author: Shiv Patel
//@ID: 1935098
package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor {

	public RemoveDuplicates(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}

	@Override
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> asRemoved = new ArrayList<String>();
		//Loops through the input ArrayList and adds every value except the duplicates to the new ArrayList asRemoved
		for (int i=0; i < input.size(); i++) {
			//If statement checks if the new ArrayList contains whatever value in input ArrayList before actually adding it
			if (asRemoved.contains(input.get(i))) {
			}
			else {
				asRemoved.add(input.get(i));
			}
		}
		return asRemoved;
	}

}