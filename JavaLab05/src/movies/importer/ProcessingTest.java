//@author: Shiv Patel
//@ID: 1935098
package movies.importer;

import java.io.IOException;

public class ProcessingTest {

	public static void main(String[] args) throws IOException{
		//Because I am on a Macbook I had to use / instead of \\
		//Created all the absolute paths
		String source = "/Users/mukeshpatel/Documents/Texts";
		String destination = "/Users/mukeshpatel/Documents/TextDestination";
		String destination2 = "/Users/mukeshpatel/Documents/RemovedDestination";
		
		//Created and executed the LowercaseProcessor
		LowercaseProcessor LowerProcessor = new LowercaseProcessor(source, destination);
		LowerProcessor.execute();
		
		//Created and executed the RemoveDuplicates
		RemoveDuplicates RemoverProcessor = new RemoveDuplicates(destination, destination2);
		RemoverProcessor.execute();
	}

}